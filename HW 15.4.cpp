#include <iostream>

void Ņalculation(int Number, bool Logic) {
	for (int i = Logic; i <= Number; i += 2)
	{
		std::cout << i << std::endl;
	}
}


void EvenOrOdd(int N) {
	(N % 2 == 0) ? Ņalculation(N, false) : Ņalculation(N, true);
}

int main()
{
    const int N = 10;

	std::cout
		<< "Number = " << N << ";\n\n"
		<< "Main function:" << std::endl;
	Ņalculation(N, false);

	std::cout
		<< "\n"
		<< "EvenOrOdd function:" << std::endl;
	EvenOrOdd(N);

    return 0;
}